package com.chenjl.mockapi.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chenjl.dto.HelloDto;
import com.chenjl.mockapi.util.HttpClientInvoker;
import com.chenjl.service.HelloService;

import lombok.extern.slf4j.Slf4j;
/**
 * 工程主页
  * 2018-6-29 19:01:31
  * @author jinlong.chen
  */
@Slf4j
@Controller
public class HomeController {
	
	@Autowired
	private HelloService helloService;
	
	
	@ResponseBody
	@RequestMapping(value="home1.htm",method=RequestMethod.GET)
	public String home(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse) {
		String str1 = helloService.hello();
		
		String str2 = helloService.sayHello("chenjinlong");
		
		HelloDto helloDto = helloService.getObject();
		
		log.info("调用远程服务,str1 : {}, str2 : {}, helloDto: {}",str1,str2,helloDto);
		return "home1";
	}
	
	@ResponseBody
	@RequestMapping(value="home2.htm",method=RequestMethod.GET)
	public String home2(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse) {
		
		helloService.methodWithHttpRequest();
		
		HttpClientInvoker httpInvoker = HttpClientInvoker.getInstance();
		Map<String,Object> datas = new HashMap<String,Object>();
		datas.put("tt",new Date().getTime());
		httpInvoker.syncGet("http://www.baidu.com",datas);
		
		return "home2";
	}
	
	@ResponseBody
	@RequestMapping(value="home3.htm",method=RequestMethod.GET)
	public String home3(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse) {
		helloService.methodWithMySQLAccess();
		
		return "home3";
	}
	
	@ResponseBody
	@RequestMapping(value="home4.htm",method=RequestMethod.GET)
	public String home4(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse) {
		helloService.methodWithException();
		return "home4";
	}
	
	@ResponseBody
	@RequestMapping(value="home5.htm",method=RequestMethod.POST)
	public String home5(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse) {
		return "home5";
	}
}