package com.chenjl.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.chenjl.datamonitor.dao.TMonitorScriptLogMapper;
import com.chenjl.datamonitor.model.TMonitorScriptLog;
import com.chenjl.dto.HelloDto;
import com.chenjl.service.HelloService;
import com.chenjl.util.HttpClientInvoker;

import lombok.extern.slf4j.Slf4j;
/**
 * Hession服务提供者实现类
 * 2014-12-22 14:32:12
 * @author chenjl
 */
@Slf4j
@Service("helloService")
public class HelloServiceImpl implements HelloService {
	
	@Autowired
	private TMonitorScriptLogMapper monitorScriptLogMapper;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	@Override
	public String hello() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			log.error("{}",e);
		}
		
		log.info("hello");
		return "hello";
	}
	@Override
	public String sayHello(String name) {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			log.error("{}",e);
		}
		
		log.info("sayHello : {} ",name);
		return "Hi , how are you. dear "+name+" !";
	}
	@Override
	public HelloDto getObject() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			log.error("{}",e);
		}
		
		log.info("getObject");
		
		HelloDto helloDto = new HelloDto();
		helloDto.setId(10);
		helloDto.setName("chenjl");
		helloDto.setCreateTime(new Date());
		return helloDto;
	}
	@Override
	public void methodWithHttpRequest() {
		HttpClientInvoker httpInvoker = HttpClientInvoker.getInstance();
		
		Map<String,Object> datas = new HashMap<String,Object>();
		datas.put("time",new Date().getTime());
		httpInvoker.syncGet("https://mcredit.ppdai.com/hs",datas);
		
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			log.error("{}",e);
		}
		
		datas.put("time",new Date().getTime());
		httpInvoker.syncGet("http://www.baidu.com",datas);
	}
	@Override
	public void methodWithMySQLAccess() {
		//insert
		TMonitorScriptLog insertTMonitorScriptLog = new TMonitorScriptLog();
		insertTMonitorScriptLog.setScriptId(1L);
		insertTMonitorScriptLog.setResult(100L);
		insertTMonitorScriptLog.setCostTime(100);
		insertTMonitorScriptLog.setCreateTime(new Date());
		insertTMonitorScriptLog.setUpdateTime(new Date());
		insertTMonitorScriptLog.setIsDeleted(false);
		monitorScriptLogMapper.insertSelective(insertTMonitorScriptLog);
		
		jdbcTemplate.update("insert into t_monitor_script_log(script_id,result,cost_time,create_time) value(1,2,2,now())");
		
		
		//update
		TMonitorScriptLog updateTMonitorScriptLog = new TMonitorScriptLog();
		updateTMonitorScriptLog.setId(1L);
		updateTMonitorScriptLog.setCostTime(101);
		updateTMonitorScriptLog.setUpdateTime(new Date());
		monitorScriptLogMapper.updateByPrimaryKeySelective(updateTMonitorScriptLog);
		
		jdbcTemplate.update("update t_monitor_script_log set result = 1 where id = 2");
		
		//select
		TMonitorScriptLog queryTMonitorScriptLog = new TMonitorScriptLog();
		queryTMonitorScriptLog.setScriptId(2L);
		queryTMonitorScriptLog.setIsDeleted(false);
		monitorScriptLogMapper.selectPageSelective(queryTMonitorScriptLog,new PageRequest(0,5));
		
		jdbcTemplate.queryForList("select * from t_monitor_script_log where is_deleted = 0 limit 0,1");
		
		
		//delete
		monitorScriptLogMapper.deleteByPrimaryKey(100L);
		
		jdbcTemplate.update("delete from t_monitor_script_log where id = 200");
	}
	@Override
	public void methodWithException() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			log.error("{}",e);
		}
		
		Integer.parseInt("1D");
	}
}