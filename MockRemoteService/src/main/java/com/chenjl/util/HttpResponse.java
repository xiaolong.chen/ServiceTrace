package com.chenjl.util;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * HttpInvoker返回对象
 * 2016-6-28 14:13:19
 * @author chenjinlong
 */
@Setter
@Getter
@ToString
public class HttpResponse {
	public static final int SERVICE_SUCCESS = 200;
	public static final int SERVICE_NOT_FOUND = 404;
	public static final int SERVICE_INTERNAL_ERROR = 500;
	//服务超时
	public static final int SERVICE_TIMEOUT = 600;
	//服务不可用，未启动
	public static final int SERVICE_NOT_AVAILABLE = 601;
	//未知错误
	public static final int SERVICE_ERROR_UNKNOW = 0;
	
	/**
	 * 状态码
	 */
	private int statusCode;
	/**
	 * 返回文本
	 */
	private String responseText;
}