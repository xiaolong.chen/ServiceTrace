
create database datamonitor;

create table t_monitor_script_log (
  id bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '物理主键', 
  script_id bigint(20) NOT NULL DEFAULT '0' COMMENT '脚本主键',
  result bigint(20) NOT NULL DEFAULT '0' COMMENT '运行结果', 
  cost_time int(8) NOT NULL DEFAULT '0' COMMENT '执行时间，单位为毫秒',  
  create_time timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '记录创建时间', 
  update_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '记录更新时间', 
  is_deleted TINYINT(1) NOT NULL DEFAULT '0' COMMENT '是否有效(0有效, 1无效)',
  primary key (id), 
  INDEX idx_script_id(script_id) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 COMMENT = '监控脚本运行日志';



