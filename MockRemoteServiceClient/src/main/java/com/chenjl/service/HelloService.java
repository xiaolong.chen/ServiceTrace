package com.chenjl.service;

import com.chenjl.dto.HelloDto;
/**
 * 测试Hession服务声明接口
 * 2014-12-22 14:32:06
 * @author chenjl
 */
public interface HelloService {
	
	public String hello();
	
	public String sayHello(String name);
	
	public HelloDto getObject();
	
	/**
	 * 方法中带有http访问
	 */
	public void methodWithHttpRequest();
	
	/**
	 * 方法中带有MySQL 访问
	 */
	public void methodWithMySQLAccess();
	
	/**
	 * 会丢出异常的方法
	 */
	public void methodWithException();
}