package com.chenjl.trace;

import lombok.extern.slf4j.Slf4j;
/**
 * 测试时间
 * 2018-9-18 16:28:44
 * @author chenjinlong
 */
@Slf4j
public class TestOnTime {
	
	public static void main(String[] args) throws InterruptedException {
		long begin = System.nanoTime();
		Thread.sleep(1*1000);
		long now = System.nanoTime();
		
		log.info("{}",(now-begin) / 1000L / 1000L);
	}
}