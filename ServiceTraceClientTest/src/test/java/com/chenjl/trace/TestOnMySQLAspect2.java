package com.chenjl.trace;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.chenjl.trace.enums.AnnotationEnum;
import com.chenjl.trace.enums.SpanTypeEnum;
import com.chenjl.trace.model.Endpoint;

import lombok.extern.slf4j.Slf4j;
/**
 * 使用aspect对MySQL操作进行trace埋点，测试 no Spring
 * 2018-2-11 14:45:13
 * @author chenjinlong
 */
@Slf4j
public class TestOnMySQLAspect2 {
	
	private static final String url = "jdbc:mysql://127.0.0.1:3306/datamonitor?zeroDateTimeBehavior=convertToNull&characterEncoding=UTF-8";
	private static final String username = "root";
	private static final String password = "root1234";

	public static void main(String[] args) {
		//mock
		Endpoint endpoint = new Endpoint();
		endpoint.setServiceName("web request");
		endpoint.setHost("");
		String spanId = Tracer.beginRoot("",endpoint,AnnotationEnum.SERVER_RECEIVE,SpanTypeEnum.HTTP_API);
		
		Connection connection = null;
		Statement stmt1 = null;
		Statement stmt2 = null;
		PreparedStatement pstmt= null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(url,username,password);

			log.info("0, [{}]",connection.getClass().getName());
			stmt1 = connection.createStatement();
			int resultCount1 = stmt1.executeUpdate("delete from t_monitor_script_log where id = 200");
			log.info("1, [{}]",resultCount1);
			
			stmt2 = connection.createStatement();
			ResultSet resultSet = stmt2.executeQuery("select * from t_monitor_script_log where is_deleted = 0 limit 0,3");
			while(resultSet.next()) {
				log.info("2, [{}]");
			}
			
			pstmt = connection.prepareStatement("update t_monitor_script_log set result = ? where id = ?");
			pstmt.setLong(1,4L);
			pstmt.setLong(2,3L);
			int resultCount3 = pstmt.executeUpdate();
			log.info("3, [{}]",resultCount3);
		}
		catch (Exception e) {
			log.error("{}",e);
		}
		finally {
			try {
				pstmt.close();
			}
			catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				stmt1.close();
			}
			catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			try {
				stmt2.close();
			}
			catch (SQLException e1) {
				e1.printStackTrace();
			}
			
			 if(connection!=null) {
				 try {
					 connection.close();
				}
				 catch (SQLException e) {
					e.printStackTrace();
				}
			 }
			 
			 Tracer.end(spanId,endpoint,AnnotationEnum.SERVER_SEND);
			 Tracer.clear();
		}
	}
}