package com.chenjl.trace;

import java.util.HashMap;
import java.util.Map;

import com.chenjl.trace.enums.AnnotationEnum;
import com.chenjl.trace.enums.SpanTypeEnum;
import com.chenjl.trace.model.Endpoint;
import com.chenjl.trace.test.http.HttpClientInvoker;
import com.chenjl.trace.test.http.HttpResponse;

import lombok.extern.slf4j.Slf4j;
/**
 * 测试Http Client Aspect埋点
 * -javaagent:aspectjweaver-1.8.7.jar
 * 
 * 2015-11-4 17:55:00
 * @author jinlong.chen
 */
@Slf4j
public class TestOnApacheHttpClient4Aspect {
	
	public static void main(String[] args) {
		//mock
		Endpoint endpoint = new Endpoint();
		endpoint.setServiceName("web request");
		endpoint.setHost("");
		String spanId = Tracer.beginRoot("",endpoint,AnnotationEnum.SERVER_RECEIVE,SpanTypeEnum.HTTP_API);
		
		//aspect埋点http
		HttpClientInvoker httpInvoker = HttpClientInvoker.getInstance();
		Map<String,Object> datas = new HashMap<String,Object>();
		String url = "https://mcredit.ppdai.com/hs";
		datas.put("platform","ios");
		datas.put("memo","test");
		HttpResponse httpResponse = httpInvoker.syncGet(url,datas);
		log.info("http response : {}",httpResponse);
		
		Tracer.end(spanId,endpoint,AnnotationEnum.SERVER_SEND);
		Tracer.clear();
	}
}