package com.chenjl.trace;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSONObject;
import com.chenjl.datamonitor.dao.TMonitorScriptLogMapper;
import com.chenjl.datamonitor.model.TMonitorScriptLog;
import com.chenjl.trace.enums.AnnotationEnum;
import com.chenjl.trace.enums.SpanTypeEnum;
import com.chenjl.trace.model.Endpoint;

import lombok.extern.slf4j.Slf4j;
/**
 * 使用aspect对MySQL操作进行trace埋点，测试 with Spring
 * 2018-2-11 14:45:13
 * @author chenjinlong
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestOnMySQLAspect {

	@Autowired
	private TMonitorScriptLogMapper monitorScriptLogMapper;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	private String spanId = null;
	@Before 
	public void before() {
		//mock
		Endpoint endpoint = new Endpoint();
		endpoint.setServiceName("web request");
		endpoint.setHost("110.120");
		spanId = Tracer.beginRoot("mock root",endpoint,AnnotationEnum.SERVER_RECEIVE,SpanTypeEnum.HTTP_API);
	}
	@After
	public void after() {
		Endpoint endpoint = new Endpoint();
		endpoint.setServiceName("web request");
		endpoint.setHost("110.120");
		Tracer.end(spanId,endpoint,AnnotationEnum.SERVER_SEND);
		Tracer.clear();
	}
	
	@Test
	public void testOnInsert() {
		TMonitorScriptLog insertTMonitorScriptLog = new TMonitorScriptLog();
		insertTMonitorScriptLog.setScriptId(1L);
		insertTMonitorScriptLog.setResult(100L);
		insertTMonitorScriptLog.setCostTime(100);
		insertTMonitorScriptLog.setCreateTime(new Date());
		insertTMonitorScriptLog.setUpdateTime(new Date());
		insertTMonitorScriptLog.setIsDeleted(false);
		monitorScriptLogMapper.insertSelective(insertTMonitorScriptLog);
		
		jdbcTemplate.update("insert into t_monitor_script_log(script_id,result,cost_time,create_time) value(1,2,2,now())");
	}
	@Test
	public void testOnUpdate() {
		TMonitorScriptLog updateTMonitorScriptLog = new TMonitorScriptLog();
		updateTMonitorScriptLog.setId(1L);
		updateTMonitorScriptLog.setCostTime(101);
		updateTMonitorScriptLog.setUpdateTime(new Date());
		monitorScriptLogMapper.updateByPrimaryKeySelective(updateTMonitorScriptLog);
		
		jdbcTemplate.update("update t_monitor_script_log set result = 1 where id = 2");
	}
	@Test
	public void testOnSelect() {
		TMonitorScriptLog queryTMonitorScriptLog = new TMonitorScriptLog();
		queryTMonitorScriptLog.setScriptId(2L);
		queryTMonitorScriptLog.setIsDeleted(false);
		List<TMonitorScriptLog> monitorScriptLogs = monitorScriptLogMapper.selectPageSelective(queryTMonitorScriptLog,new PageRequest(0,5));
		log.info("testOnSelect : {}",JSONObject.toJSONString(monitorScriptLogs,true));
		
		jdbcTemplate.queryForList("select * from t_monitor_script_log where is_deleted = 0 limit 0,1");
	}
	@Test
	public void testOnDelete() {
		monitorScriptLogMapper.deleteByPrimaryKey(100L);
		
		jdbcTemplate.update("delete from t_monitor_script_log where id = 200");
	}
}