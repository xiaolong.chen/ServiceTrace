package com.chenjl.trace;

import java.net.MalformedURLException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.chenjl.trace.enums.AnnotationEnum;
import com.chenjl.trace.enums.SpanTypeEnum;
import com.chenjl.trace.model.Endpoint;
import com.chenjl.trace.test.service.HelloService;

import lombok.extern.slf4j.Slf4j;
/**
 * 测试Hessian Client Aspect埋点
 * -javaagent:aspectjweaver-1.8.7.jar
 * 
 * 2015-11-4 17:55:00
 * @author jinlong.chen
 */
@Slf4j
public class TestOnHessianAspect {
	
	public static void main(String[] args) throws MalformedURLException {
		//mock
		Endpoint endpoint = new Endpoint();
		endpoint.setServiceName("web request");
		endpoint.setHost("");
		String spanId = Tracer.beginRoot("",endpoint,AnnotationEnum.SERVER_RECEIVE,SpanTypeEnum.HTTP_API);
		
		try {
			String url = "http://localhost:8080/mock-remote-service/helloService.hessian";
			
			HessianProxyFactory hessianProxyFactory = new HessianProxyFactory();
			hessianProxyFactory.setOverloadEnabled(false);
			HelloService helloService = (HelloService) hessianProxyFactory.create(HelloService.class,url);
			
			log.info("Hessian："+helloService.hello());
			log.info("Hessian："+helloService.sayHello("chenjl"));
			log.info("Hessian："+helloService.getObject());
		}
		finally {
			Tracer.end(spanId,endpoint,AnnotationEnum.SERVER_SEND);
			Tracer.clear();
		}
	}
}