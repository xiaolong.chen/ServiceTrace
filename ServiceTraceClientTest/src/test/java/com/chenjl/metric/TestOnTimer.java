package com.chenjl.metric;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

import lombok.extern.slf4j.Slf4j;
/**
 * Timers（计时器）主要是用来统计某一块代码段的执行时间以及其分布情况，具体是基于Histograms和Meters来实现的
 * 
 * 2018-9-12 11:19:19
 * @author chenjinlong
 */
@Slf4j
public class TestOnTimer {
	
	private static final MetricRegistry metricRegistry = new MetricRegistry();
	/**
	 *  在控制台上打印输出
	 */
	private static final ConsoleReporter consoleReporter = ConsoleReporter.forRegistry(metricRegistry).build();
	
	
	public static void main(String[] args) {
		consoleReporter.start(3,TimeUnit.SECONDS);
		
		Timer timer = metricRegistry.timer(MetricRegistry.name(TestOnTimer.class, "request"));
		
		
		Random random = new Random();
		for(int i=0;i<30;i++) {
			Timer.Context context = timer.time();
			
			try {
				int seed = random.nextInt(5000)+1;
				Thread.sleep(seed);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			context.stop();
		}
		
		log.info("-->>--end-->>--");
	}
}