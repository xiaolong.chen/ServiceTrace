package com.chenjl.metric;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;

import lombok.extern.slf4j.Slf4j;
/**
 * Histograms（直方图数据）主要使用来统计数据的分布情况，最大值、最小值、平均值、中位数，百分比（75%、90%、95%、98%、99%和99.9%）。
 * 如需要统计某个页面的请求响应时间分布情况，可以使用该种类型的Metrics进行统计。
 * 
 * 2018-9-12 11:19:19
 * @author chenjinlong
 */
@Slf4j
public class TestOnHistograms {
	
	private static final MetricRegistry metricRegistry = new MetricRegistry();
	/**
	 *  在控制台上打印输出
	 */
	private static final ConsoleReporter consoleReporter = ConsoleReporter.forRegistry(metricRegistry).build();
	
	
	public static void main(String[] args) {
		consoleReporter.start(3,TimeUnit.SECONDS);
		
		Histogram histogram = metricRegistry.histogram(MetricRegistry.name(TestOnHistograms.class,"random"));
		
		Random random = new Random();
		for(int i=0;i<30;i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int seed = random.nextInt(1000)+1;
			
			histogram.update(seed);
		}
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		log.info("-->>--end-->>--");
	}
}