package com.chenjl.metric;

import java.util.concurrent.TimeUnit;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;

import lombok.extern.slf4j.Slf4j;
/**
 * Counter（计数器）是Gauge的一个特例，维护一个计数器，可以通过inc()和dec()方法对计数器做修改
 * 
 * 2018-9-12 10:52:09
 * @author chenjinlong
 */
@Slf4j
public class TestOnCounter {
	
	private static final MetricRegistry metricRegistry = new MetricRegistry();
	/**
	 *  在控制台上打印输出
	 */
	private static final ConsoleReporter consoleReporter = ConsoleReporter.forRegistry(metricRegistry).build();
	
	
	public static void main(String[] args) {
		consoleReporter.start(3,TimeUnit.SECONDS);
		
		//注册到容器中
		Counter counter = new Counter();
		metricRegistry.register(MetricRegistry.name(TestOnCounter.class,"pending-jobs"),counter);
		
		for(int i=0;i<30;i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//counter.inc();
			counter.dec();
		}
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		log.info("-->>--end-->>--");
	}
}