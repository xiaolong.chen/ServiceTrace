package com.chenjl.metric;

import java.util.Date;
import java.util.Map.Entry;
import java.util.Random;

import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheck.Result;
import com.codahale.metrics.health.HealthCheckRegistry;

import lombok.extern.slf4j.Slf4j;
/**
 * Metrics提供了一个独立的模块：Health Checks，用于对Application、其子模块或者关联模块的运行是否正常做检测。
 * 健康检查，如心跳
 * 
 * 2018-9-12 15:45:00
 * @author chenjinlong
 */
@Slf4j
public class TestOnHealthCheck {

	public static class THealthCheck extends HealthCheck {
		private Random random = new Random();
		
		@Override
		protected Result check() throws Exception {
			int seed = random.nextInt(10)+1;
			if(seed > 7) {
				return Result.unhealthy("unhealthy "+new Date().toString());
			}
			else {
				return Result.healthy();
			}
		}
	}
	
	public static void main(String[] args) {
		
		HealthCheckRegistry healthCheckRegistry = new HealthCheckRegistry();
		healthCheckRegistry.register("oneHealthCheck",new THealthCheck());
		
		for(int i=0;i<10;i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			for (Entry<String,Result> entry : healthCheckRegistry.runHealthChecks().entrySet()) {
				String key = entry.getKey();
				Result result = entry.getValue();
				
				if(result.isHealthy()) {
					log.info("key : {} isHealthy", key );
				}
				else {
					log.info("key : {} not Healthy, message : {} , e: {}", key,result.getMessage(),result.getError());
				}
			}
		}
		
		log.info("-->>--end-->>--");
	}
}