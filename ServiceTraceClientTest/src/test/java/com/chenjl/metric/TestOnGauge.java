package com.chenjl.metric;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;

import lombok.extern.slf4j.Slf4j;
/**
 * Metrics（度量）是一个给JAVA服务的各项指标提供度量工具的包，在JAVA代码中嵌入Metrics代码，可以方便的对业务代码的各个指标进行监控；
 * Metrics能够很好的跟Ganlia、Graphite结合，方便的提供图形化接口。
 * 
 * Gauges是一个最简单的计量，一般用来统计瞬时状态的值，比如系统中处于pending状态的job
 * 
 * 2018-9-12 10:52:09
 * @author chenjinlong
 */
@Slf4j
public class TestOnGauge {
	
	private static final MetricRegistry metricRegistry = new MetricRegistry();
	/**
	 *  在控制台上打印输出
	 */
	private static final ConsoleReporter consoleReporter = ConsoleReporter.forRegistry(metricRegistry).build();
	
	private static final AtomicLong al = new AtomicLong(0);
	
	
	public static void main(String[] args) {
		consoleReporter.start(3,TimeUnit.SECONDS);
		
		Gauge<Integer> gauge = new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				//直接返回某个值
				return al.intValue();
			}
		};
		
		//注册到容器中
		metricRegistry.register(MetricRegistry.name(TestOnGauge.class,"pending-job","size"), gauge);
		
		//测试JMX
		JmxReporter jmxReporter = JmxReporter.forRegistry(metricRegistry).build();
		jmxReporter.start();
		
		for(int i=0;i<30;i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			al.incrementAndGet();
		}
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		log.info("-->>--end-->>--");
	}
}