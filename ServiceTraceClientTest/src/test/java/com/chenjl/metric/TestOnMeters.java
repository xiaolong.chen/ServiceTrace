package com.chenjl.metric;

import java.util.concurrent.TimeUnit;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;

import lombok.extern.slf4j.Slf4j;
/**
 * Meters（TPS计算器）用来度量某个时间段的平均处理次数（request per second），每1、5、15分钟的TPS；
 * 比如一个service的请求数，通过metrics.meter()实例化一个Meter之后，然后通过meter.mark()方法就能将本次请求记录下来。
 * 统计结果有总的请求数，平均每秒的请求数，以及最近的1、5、15分钟的平均TPS
 * 
 * 2018-9-12 11:19:19
 * @author chenjinlong
 */
@Slf4j
public class TestOnMeters {
	
	private static final MetricRegistry metricRegistry = new MetricRegistry();
	/**
	 *  在控制台上打印输出
	 */
	private static final ConsoleReporter consoleReporter = ConsoleReporter.forRegistry(metricRegistry).build();
	
	
	public static void main(String[] args) {
		consoleReporter.start(3,TimeUnit.SECONDS);
		
		Meter meter = new Meter();
		metricRegistry.register(MetricRegistry.name(TestOnMeters.class,"request"),meter);
		
		for(int i=0;i<30;i++) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//记录状态
			meter.mark();
		}
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		log.info("-->>--end-->>--");
	}
}