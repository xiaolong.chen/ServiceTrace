package com.chenjl.trace.test;

import java.lang.instrument.Instrumentation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 在class 被加载之前对其拦截，自定义加强的字节码
 * 要自定义MANIFEST.MF文件，指定Premain-Class
 * 
 * 2018-8-31 15:26:44
 * @author chenjinlong
 */
public class TestAgent {
	private static final Logger log = LoggerFactory.getLogger(TestAgent.class);
	
	
	/**
	 * 在main方法之前运行，与main方法运行在同一个JVM实例中
	 * 被同一个System ClassLoader装载
	 * 被统一的安全策略(security policy)和上下文(context)管理
	 * @param agentArgs
	 * @param instrumentation
	 */
	public static void premain(String agentArgs,Instrumentation instrumentation) {
		log.info("premain invoke... agentArgs : [{}]",agentArgs);
		
		instrumentation.addTransformer(new TestClassFileTransformer());
	}
}