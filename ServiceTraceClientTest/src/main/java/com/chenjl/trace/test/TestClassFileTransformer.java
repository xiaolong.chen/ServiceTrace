package com.chenjl.trace.test;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 类字节码转换器
 * 每次新加载类到ClassLoader都会调用
 * 2018-8-31 16:40:22
 * @author chenjinlong
 */
public class TestClassFileTransformer implements ClassFileTransformer {
	private static final Logger log = LoggerFactory.getLogger(TestClassFileTransformer.class);
	
	/**
	 * transform用来转换提供的类字节码，并返回一个新的替换字节码。
	 * 不同于CGLIB、JDK动态代理等字节码操作技术，ClassFileTransformer是彻底的替换掉原类，
	 * 而CGLIB和JDK动态代理是生成一个新子类或接口实现
	 */
	@Override
	public byte[] transform(ClassLoader classLoader, String fullyQualifiedClassName, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		String className = fullyQualifiedClassName.replace("/", ".");
		log.info("classLoader : {}, className : {}",classLoader,className);
		
		return classfileBuffer;
	}
}