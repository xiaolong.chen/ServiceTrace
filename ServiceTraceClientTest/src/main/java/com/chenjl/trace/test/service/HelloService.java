package com.chenjl.trace.test.service;
/**
 * 测试Hession服务声明接口
 * 2014-12-22 14:32:06
 * @author chenjl
 */
public interface HelloService {
	
	public String hello();
	
	public String sayHello(String name);
	
	public HelloDto getObject();
}