package com.chenjl.trace.test.service;

import java.util.Date;

import lombok.extern.slf4j.Slf4j;
/**
 * Hession服务提供者实现类
 * 2014-12-22 14:32:12
 * @author chenjl
 */
@Slf4j
public class HelloServiceImpl implements HelloService {
	
	@Override
	public String hello() {
		log.info("hello");
		return "hello";
	}
	@Override
	public String sayHello(String name) {
		log.info("sayHello : {} ",name);
		//Integer.parseInt("1D");
		return "Hi , how are you. dear "+name+" !";
	}
	@Override
	public HelloDto getObject() {
		log.info("getObject");
		
		HelloDto helloDto = new HelloDto();
		helloDto.setId(10);
		helloDto.setName("chenjl");
		helloDto.setCreateTime(new Date());
		return helloDto;
	}
}