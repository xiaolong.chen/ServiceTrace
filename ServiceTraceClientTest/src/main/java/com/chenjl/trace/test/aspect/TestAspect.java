package com.chenjl.trace.test.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 定义增强的切面
 * LTW（Load Time Weaver），加载期切面织入，是ApsectJ切面织入的一种方式
 * 通过JVM代理在类加载期替换字节码到达织入切面的目的
 * 
 * 2018-9-3 15:10:18
 * @author chenjinlong
 */
@Aspect
public class TestAspect {
	private static final Logger log = LoggerFactory.getLogger(TestAspect.class);
	
	@Pointcut("execution(public * com.chenjl.trace.test.service.HelloServiceImpl.*(..))")
	public void getPointCut() {
		
	}
	
	@Around(value="getPointCut()")
	public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		Object result = null;
		Throwable throwable = null;
		
		String methodName = proceedingJoinPoint.getSignature().getName();
		log.info(" -->-- invoke begin : {}",methodName);
		
		try {
			result = proceedingJoinPoint.proceed();
		}
		catch (Throwable e) {
			throwable = e;
		}
		
		log.info(" -->-- invoke end : {}，exception: {}",methodName,throwable != null);
		
		if(throwable != null) {
			throw new Throwable(throwable);
		}
		return result;
	}
}