package com.chenjl.trace.test.protobuf;

import com.chenjl.trace.protobuf.TraceProtobuf.ProtoAnnotation;
import com.chenjl.trace.protobuf.TraceProtobuf.ProtoBinaryAnnotation;
import com.chenjl.trace.protobuf.TraceProtobuf.ProtoEndpoint;
import com.chenjl.trace.protobuf.TraceProtobuf.ProtoSpan;
import com.google.protobuf.InvalidProtocolBufferException;

import lombok.extern.slf4j.Slf4j;
/**
 * 测试TraceProtobuf
 * 2018-9-14 17:41:30
 * @author chenjinlong
 */
@Slf4j
public class TestOnTraceProtobuf {

	public static void main(String[] args) throws InvalidProtocolBufferException {
		
		ProtoEndpoint.Builder protoEndpointBuilder = ProtoEndpoint.newBuilder();
		protoEndpointBuilder.setServiceName("a");
		protoEndpointBuilder.setHost("b");
		protoEndpointBuilder.setPort(8080);
		
		ProtoAnnotation.Builder protoAnnotationBuilder = ProtoAnnotation.newBuilder();
		protoAnnotationBuilder.setEndpoint(protoEndpointBuilder.build());
		protoAnnotationBuilder.setTimestamp(System.currentTimeMillis());
		protoAnnotationBuilder.setValue("rc");
		
		ProtoBinaryAnnotation.Builder protoBinaryAnnotationBuilder = ProtoBinaryAnnotation.newBuilder();
		protoBinaryAnnotationBuilder.setEndpoint(protoEndpointBuilder.build());
		protoBinaryAnnotationBuilder.setKey("http.code");
		protoBinaryAnnotationBuilder.setValue("200");
		
		ProtoSpan.Builder protoSpanBuilder  = ProtoSpan.newBuilder();
		protoSpanBuilder.setTraceId("1");
		protoSpanBuilder.setParentId("1");
		protoSpanBuilder.setId("2");
		protoSpanBuilder.setName("sss");
		protoSpanBuilder.setSpanType("http api");
		protoSpanBuilder.setTimestamp(System.currentTimeMillis());
		protoSpanBuilder.setDuration(System.currentTimeMillis());
		protoSpanBuilder.addAnnotations(protoAnnotationBuilder.build());
		protoSpanBuilder.addBinaryAnnotations(protoBinaryAnnotationBuilder.build());
		protoSpanBuilder.setDomainName("mock-service-api");
		protoSpanBuilder.setIp("192.168.0.10");
		
		ProtoSpan protoSpan = protoSpanBuilder.build();
		byte[] bytes = protoSpan.toByteArray();
		log.info("bytes length : {}",bytes.length);
		
		//反序列化
		ProtoSpan newProtoSpan = ProtoSpan.parseFrom(bytes);
		log.info("{}",newProtoSpan.toString());
	}
}