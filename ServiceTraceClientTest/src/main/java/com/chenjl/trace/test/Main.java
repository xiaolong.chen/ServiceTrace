package com.chenjl.trace.test;

import com.chenjl.trace.test.service.HelloService;
import com.chenjl.trace.test.service.HelloServiceImpl;

import lombok.extern.slf4j.Slf4j;
/**
 * 织入加强的字节码，运行jar的main函数
 * 
 * 运行自定义的javaagent
 * java -javaagent:ServiceTraceClientTest.jar=Hello. -jar ServiceTraceClientTest.jar
 * 
 * 运行aspectj的javaagent
 * -javaagent:aspectjweaver-1.8.7.jar
 *  java -javaagent:./lib/aspectjweaver-1.8.7.jar -jar ServiceTraceClientTest.jar
 * 
 * 2018-8-31 16:23:02
 * @author chenjinlong
 */
@Slf4j
public class Main {
	
	public static void main(String[] args) {
		log.info("哈哈哈");
		
		//aspect增强service
		HelloService helloService = new HelloServiceImpl();
		log.info("1: {}",helloService.hello());
		log.info("2: {}",helloService.sayHello("chenjinlong"));
		log.info("3: {}",helloService.getObject());
	}
}