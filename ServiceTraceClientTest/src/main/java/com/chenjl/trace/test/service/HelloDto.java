package com.chenjl.trace.test.service;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * 测试Hession实体类
 * 2014-12-22 14:51:12
 * @author chenjl
 */
@Getter
@Setter
@ToString
public class HelloDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id = 0;
	private String name = null;
	private Date createTime;
}