package com.chenjl.datamonitor.model;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 框架自动生成表模型和CRUD操作，勿修改；
 * 如特殊需要，请以Ext***Mapper自行扩展；
 * 生成日期 : 2018-08-06 17:03:48
 * @author chenjinlong
 */
@Setter
@Getter
@ToString
public class TMonitorScriptLog {
    private Long id;

    private Long scriptId;

    private Long result;

    private Integer costTime;

    private Date createTime;

    private Date updateTime;

    private Boolean isDeleted;
}