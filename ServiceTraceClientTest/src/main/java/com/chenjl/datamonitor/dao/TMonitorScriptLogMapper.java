package com.chenjl.datamonitor.dao;

import com.chenjl.datamonitor.model.TMonitorScriptLog;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 * 框架自动生成表模型和CRUD操作，勿修改；
 * 如特殊需要，请以Ext***Mapper自行扩展；
 * 生成日期 : 2018-07-12 10:37:52
 * @author chenjinlong
 */
@Repository
public interface TMonitorScriptLogMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TMonitorScriptLog record);

    int insertSelective(TMonitorScriptLog record);

    TMonitorScriptLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TMonitorScriptLog record);

    int updateByPrimaryKey(TMonitorScriptLog record);

    /**
     * 通用查询行数方法，可设置查询属性
     */
    Long selectCountSelective(@Param("record") TMonitorScriptLog record);

    /**
     * 通用查询List方法，可设置查询属性，排序字段，分页参数 
     */
    List<TMonitorScriptLog> selectPageSelective(@Param("record") TMonitorScriptLog record, @Param("pageable") Pageable pageable);

    /**
     * 批量逻辑删除,set is_deleted=1返回影响行数
     */
    Integer batchDeleteByPrimaryKey(List<Long> pkIds);

    /**
     * 插入行记录返回自增ID
     */
    Integer insertSelectiveReturnPrimaryKey(TMonitorScriptLog record);

    /**
     * 批量插入行数据返回插入行数
     */
    Integer batchInsert(List<TMonitorScriptLog> records);

    /**
     * 根据主键批量获取
     */
    List<TMonitorScriptLog> batchSelectByPrimaryKey(List<Long> pkIds);
}