package com.chenjl.trace;

import java.net.InetAddress;
import java.net.UnknownHostException;
/**
 * 全局变量
 * 2018-9-25 19:20:40
 * @author chenjinlong
 */
public class Constant {

	public static final String BROKER_URL = "dev.cjl.com:9092";
	public static final String TOPIC_NAME_STR = "topic_test_str";
	
	public static final String TOPIC_NAME_PROTOBUF = "topic_test_protobuf";
	
	
	
	public static void main(String[] args) throws UnknownHostException {
		System.out.println(InetAddress.getLocalHost().getHostAddress());
	}
}