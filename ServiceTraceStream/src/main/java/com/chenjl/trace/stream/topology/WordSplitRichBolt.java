package com.chenjl.trace.stream.topology;

import java.util.Map;

import com.chenjl.trace.stream.util.Constant;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
/**
 * Bolt进行计算获取句子，将单词分割
 * 2018-8-16 20:02:12
 * @author chenjinlong
 */
@Slf4j
public class WordSplitRichBolt implements IRichBolt {
	private static final long serialVersionUID = 1L;
	
	private JedisPool jedisPool = null;
	private String redisHost = "10.113.31.109";
	private Integer redisPort = 6379;
	
	private OutputCollector outputCollector ;
	
	
	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext topologyContext, OutputCollector outputCollector) {
		log.info("IRichBolt prepare~~");
		this.outputCollector = outputCollector;
		
		this.jedisPool = new JedisPool(redisHost,redisPort);
	}
	@Override
	public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
		log.info("IRichBolt declareOutputFields~~");
		outputFieldsDeclarer.declare(new Fields(Constant.WORD_FIELD));
	}
	@Override
	public void execute(Tuple tuple) {
		String sentence = tuple.getStringByField(Constant.SENTENCES_FIELD);
		log.info("WordSplitRichBolt receive sentence : {}",sentence);
		
		Jedis jedis = jedisPool.getResource();
		jedis.lpush(Constant.SENTENCES_FIELD,sentence);
		jedis.close();
		
		String[] words = sentence.split(" ");
		for(String word : words) {
			outputCollector.emit(new Values(word));
		}
		
		/**
		 * 如果继承的是IRichBolt，则需要手动ack； BaseRichBolt会自动应答；
		 * 所有的tuple都必须在一定时间内应答, 可以是ack或者fail
		 * 否则，spout就会重发tuple
		 */
		outputCollector.ack(tuple);
	}
	@Override
	public void cleanup() {
		log.info("WordSplitRichBolt cleanup");
	}
	@Override
	public Map<String, Object> getComponentConfiguration() {
		log.info("IRichBolt getComponentConfiguration~~");
		return null;
	}
}