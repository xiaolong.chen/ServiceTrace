package com.chenjl.trace.stream.util;
/**
 * 全链路监控流常量
 * 2019-1-10 15:45:33
 * @author chenjinlong
 */
public class Constant {
	public static final String TOPOLOGY_NAME = "TraceTopology-V2";
	
	public static final String SENTENCESRICHSPOUT_ID = "chenjl_SentencesRichSpout";
	public static final String WORDSPLITRICHBOLT_ID = "chenjlt_WordSplitRichBolt";
	public static final String WORDCOUNTRICHBOLT_ID = "chenjlt_WordCountRichBolt";
	
	/**
	 * Spout，Bolt传递字段名称
	 */
	public static final String SENTENCES_FIELD="sentence";
	public static final String WORD_FIELD="word";
	
	
	/**
	 * 获取spout的并发设置
	 */
	public static final int spoutParal = 1;
	/**
	 * 获取bolt的并发设置
	 */
	public static final int boltParal = 1;
	/**
	 *  
	 */
	public static final int WORKER_NUM = 3;
}