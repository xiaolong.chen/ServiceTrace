package com.chenjl.trace.context;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.chenjl.trace.model.Span;

import lombok.Getter;
import lombok.Setter;
/**
 * 追踪上下文
 * 2018-9-4 17:33:02
 * @author chenjinlong
 */
@Setter
@Getter
public class TraceContext {
	private Span root;
	private Map<String,Span> spanMap = new LinkedHashMap<String,Span>();
	
	
	/**
	 * 添加span
	 * @param span
	 */
	public void addSpan(Span span) {
		if(root == null) {
			root = span;
		}
		
		spanMap.put(span.getId(),span);
	}
	
	/**
	 * 将spans输出到Log
	 * @return
	 */
	public Collection<Span> getSpans() {
		return spanMap.values();
	}
}