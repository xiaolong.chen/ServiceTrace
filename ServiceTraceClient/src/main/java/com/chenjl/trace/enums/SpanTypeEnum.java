package com.chenjl.trace.enums;

import lombok.Getter;
/**
 * span类型枚举
 * 2018-9-5 20:38:14
 * @author chenjinlong
 */
@Getter
public enum SpanTypeEnum {

	HTTP_API("http_api","HTTP服务接口"),
	DUBBO_SERVICE("dubbo_service","Dubbo RPC"),
	HESSIAN_SERVICE("hessian_service","Hessian RPC"),
	APACHE_HTTPCLIENT4("apache_httpclient4","httpclient4 发起外部访问"),
	MYSQL_ACCESS("mysql_access","MySQL CRUD"),
	;
	
	private String value;
	private String desc;
	
	private SpanTypeEnum(String value,String desc) {
		this.value = value;
		this.desc = desc;
	}
}