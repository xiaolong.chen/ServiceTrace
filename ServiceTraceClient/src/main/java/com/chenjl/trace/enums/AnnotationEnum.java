package com.chenjl.trace.enums;

import lombok.Getter;
/**
 * span注解枚举
 * 2018-9-5 20:38:14
 * @author chenjinlong
 */
@Getter
public enum AnnotationEnum {

	CLIENT_SEND("cs","客户端发起请求"),
	SERVER_RECEIVE("sr","服务器接受请求，开始处理"),
	SERVER_SEND("ss","服务器完成处理，给客户端应答"),
	CLIENT_RECEIVE("cr","客户端接收服务端的应答"),
	;
	
	private String value;
	private String desc;
	
	private AnnotationEnum(String value,String desc) {
		this.value = value;
		this.desc = desc;
	}
}