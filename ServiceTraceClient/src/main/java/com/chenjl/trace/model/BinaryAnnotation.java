package com.chenjl.trace.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * 记录事件额外的RPC信息
 * 2018-9-4 17:12:00
 * @author chenjinlong
 */
@Getter
@Setter
@ToString
public class BinaryAnnotation {
	
	private Endpoint endpoint;
	
	/**
	 * 额外信息的键值对
	 */
	private String key;
	private String value;
}