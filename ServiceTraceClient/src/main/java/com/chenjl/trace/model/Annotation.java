package com.chenjl.trace.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * span单元的事件记录，成对出现
 * 2018-9-4 17:12:00
 * @author chenjinlong
 */
@Getter
@Setter
@ToString
public class Annotation {
	
	private Endpoint endpoint;
	/**
	 * 发生事件的时间戳
	 */
	private Long timestamp;
	/**
	 * 事件类型
	 * 客户端发起请求 - cs
	 * 服务器接受请求，开始处理 - sr
	 * 服务器完成处理，给客户端应答 - ss
	 * 客户端接收服务端的应答 - cr
	 */
	private String value;
}