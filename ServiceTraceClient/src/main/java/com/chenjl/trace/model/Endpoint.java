package com.chenjl.trace.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * 端点
 * 2018-9-4 17:09:20
 * @author chenjinlong
 */
@Getter
@Setter
@ToString
public class Endpoint {
	/**
	 * httpclient4 request
	 * hessian request
	 * mysql access
	 * dubbo service
	 * web request
	 */
	private String serviceName;
	
	private String host;
	private Integer port;
}