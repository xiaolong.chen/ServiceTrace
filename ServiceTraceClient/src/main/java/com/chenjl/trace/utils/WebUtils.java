package com.chenjl.trace.utils;

import javax.servlet.http.HttpServletRequest;
/**
 * web公用方法
 * 2018-9-4 17:41:42
 * @author chenjinlong
 */
public class WebUtils {
	
	
	/**
	 * 从http请求获取实际的ip地址
	 * @param httpServletRequest
	 * @return
	 */
	public static final String getRequestIp(HttpServletRequest httpServletRequest) {
		String ip = httpServletRequest.getHeader("X-Forwarded-For");
		if(ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
			ip = httpServletRequest.getHeader("X-Real-IP");
        }
		
		if(ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = httpServletRequest.getHeader("Proxy-Client-IP");
        }
		
        if(ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = httpServletRequest.getHeader("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.isEmpty()) {
        	ip = httpServletRequest.getRemoteAddr();
        }
        
        return ip;
    }
	
}