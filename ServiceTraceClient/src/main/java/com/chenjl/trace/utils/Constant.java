package com.chenjl.trace.utils;
/**
 * 常量
 * 2018-6-29 18:47:27
 * @author chenjinlong
 */
public final class Constant {
	
	public static final String TRACE_ID = "traceId";
	public static final String TRACE_PARENT_ID = "parentId";
	
	/**
	 * 用于配置slf4j MDC
	 */
	public static final String C_TRACE_ID = "cTraceId";
	
}