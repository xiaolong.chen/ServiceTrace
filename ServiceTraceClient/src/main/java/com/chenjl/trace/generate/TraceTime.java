package com.chenjl.trace.generate;
/**
 * 获取当前时间
 * 2018-9-5 20:17:54
 * @author chenjinlong
 */
public class TraceTime {

	/**
	 * 获取当前的时间
	 * @return
	 */
	public static final long get() {
		 return System.nanoTime();
	}
}