package com.chenjl.trace.generate;

import java.util.UUID;
/**
 * ID生成器
 * 2018-9-5 16:42:58
 * @author chenjinlong
 */
public class IdGenerator {
	
	/**
	 * 根据UUID获取全局唯一Id
	 * @return
	 */
	public static final String get() {	
		return String.valueOf(UUID.randomUUID().getLeastSignificantBits()).replace("-", "");
	}
}