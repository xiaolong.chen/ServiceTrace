package com.chenjl.trace.client;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
/**
 * 用于追踪http api获取httpStatus, 200, 500, 404 等，HttpServletResponse包装类
 * 2018-9-20 13:30:24
 * @author chenjinlong
 */
public class TraceHttpServletResponse extends HttpServletResponseWrapper {
	/**
	 * 状态码，200，500，404等
	 */
	private int httpStatus;
	
	public TraceHttpServletResponse(HttpServletResponse response) {
		super(response);
	}

	@Override
	public void sendError(int sc, String msg) throws IOException {
		this.httpStatus = sc;
		super.sendError(sc, msg);
	}
	@Override
	public void sendError(int sc) throws IOException {
		this.httpStatus = sc;
		super.sendError(sc);
	}
	@Override
	public void setStatus(int sc) {
		this.httpStatus = sc;
		super.setStatus(sc);
	}
	@Override
	public void setStatus(int sc, String sm) {
		this.httpStatus = sc;
		super.setStatus(sc, sm);
	}
	
	/**
	 * Wrapper method
	 * @return
	 */
	public int getHttpStatus() {
		return httpStatus;
	}
}