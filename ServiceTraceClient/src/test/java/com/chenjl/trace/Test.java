package com.chenjl.trace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * test
 * 2018-9-7 15:08:08
 * @author chenjinlong
 */
public class Test {
	private static final Logger log = LoggerFactory.getLogger(Test.class);
	
	
	public static void main(String[] args) {
		log.info("Test");
	}
}